import {
  notEmptyValidator,
  lengthValidator,
  urlValidator,
  numberValidator,
  imageValidator,
} from "./validators";

describe("validation helpers tests", () => {
  describe("notEmptyValidator", () => {
    it("should return false when inserted value is empty", () => {
      const result = notEmptyValidator.isSatisfied("");

      expect(result).toBe(false);
    });

    it("should return true when inserted value is a truthy value", () => {
      const result = notEmptyValidator.isSatisfied("whatever123");

      expect(result).toBe(true);
    });
  });

  describe("urlValidator", () => {
    it("should return false when inserted value is not a valid url", () => {
      const result = urlValidator.isSatisfied("@gmail.com");

      expect(result).toBe(false);
    });

    it("should return true when inserted value is a valid url format", () => {
      const result = urlValidator.isSatisfied("http://google.com");

      expect(result).toBe(true);
    });
  });

  describe("lengthValidator", () => {
    it("should return false when inserted value is too short", () => {
      const result = lengthValidator(5, 10).isSatisfied("test");

      expect(result).toBe(false);
    });

    it("should return false when inserted value is too long", () => {
      const result = lengthValidator(5, 6).isSatisfied("whatever12345");

      expect(result).toBe(false);
    });

    it("should return true when inserted value is between 5 and 10 chars", () => {
      const result = lengthValidator(5, 10).isSatisfied("goodie");

      expect(result).toBe(true);
    });
  });

  describe("numberValidator", () => {
    it("should return false when inserted value is not a number", () => {
      const result = numberValidator.isSatisfied("random string");

      expect(result).toBe(false);
    });

    it("should return true when inserted value is a number", () => {
      const result = numberValidator.isSatisfied(1024);

      expect(result).toBe(true);
    });

    it("should return true when inserted value is a number as a string value", () => {
      const result = numberValidator.isSatisfied("1024");

      expect(result).toBe(true);
    });
  });

  describe("imageValidator", () => {
    it("should return false when inserted value doesn't end with .png", () => {
      const result = imageValidator.isSatisfied(".avi");

      expect(result).toBe(false);
    });

    it("should return true when inserted value is ends with image format", () => {
      const result = imageValidator.isSatisfied(".gif");

      expect(result).toBe(true);
    });
  });
});
