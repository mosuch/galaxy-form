export const notEmptyValidator = {
  errorMessage: "This field cannot be empty",
  isSatisfied: (value) => !!value.length,
};

export const lengthValidator = (min, max) => ({
  errorMessage: `This field does not meet criteria: minimum: ${min}, maximum: ${max} characters`,
  isSatisfied: (value) => value.length >= min && value.length <= max,
});

export const urlValidator = {
  errorMessage: "Invalid url format",
  isSatisfied: (value) =>
    !!value.match(
      /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/
    ),
};

export const numberValidator = {
  errorMessage: "This value must be a number",
  isSatisfied: (value) => Number.isInteger(parseInt(value)),
};

export const imageValidator = {
  errorMessage: "Provided link is not pointing to image",
  isSatisfied: (value) => !!value.match(/\.(jpg|jpeg|png|webp|avif|gif|svg)$/),
};
