import React from "react";
import ReactDOM from "react-dom/client";
import { App } from "./App";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";
import { formReducer } from "./store/reducers/form";
import "./index.css";

const reducers = combineReducers({ formReducer });

const store = createStore(reducers);

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);
