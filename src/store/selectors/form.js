import { createSelector } from "reselect";

const formState = (state) => state.formReducer;

export const formFieldsSelector = createSelector(
  formState,
  ({ formFields }) => formFields
);

export const isFormValidSelector = createSelector(formState, ({ formFields }) =>
  formFields.every((field) => field.isValid)
);

export const submissionsSelector = createSelector(
  formState,
  ({ submissions }) => submissions
);
