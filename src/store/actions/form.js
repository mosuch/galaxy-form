export const SEND_FORM = "SEND_FORM";
export const UPDATE_FORM = "UPDATE_FORM";

export const sendForm = () => ({
  type: SEND_FORM,
});

export const updateForm = (input) => ({
  type: UPDATE_FORM,
  input,
});
