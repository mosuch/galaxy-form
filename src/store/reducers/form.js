import { SEND_FORM, UPDATE_FORM } from "../actions/form";

const initialState = {
  formFields: [
    {
      inputName: "name",
      isRequired: true,
      isValid: false,
      inputValue: "",
    },
    {
      inputName: "travelReason",
      isRequired: true,
      isValid: false,
      inputValue: "",
    },
    {
      inputName: "destinationGalaxy",
      isRequired: true,
      isValid: false,
      inputValue: "",
    },
    {
      inputName: "travelerId",
      isRequired: true,
      isValid: false,
      inputValue: "",
    },
    {
      inputName: "spaceDrivingLicense",
      isRequired: true,
      isValid: false,
      inputValue: "",
    },
    {
      inputName: "travelDuration",
      isRequired: true,
      isValid: false,
      inputValue: "",
    },
  ],
  submissions: [],
};

export const formReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEND_FORM:
      return {
        ...initialState,
        submissions: [
          ...state.submissions,
          state.formFields.reduce((acc, f) => {
            Object.assign(acc, {
              [f.inputName]: f.inputValue,
            });

            return acc;
          }, {}),
        ],
      };

    case UPDATE_FORM:
      const updatedInput = {
        inputName: action.input.name,
        inputValue: action.input.value,
        isRequired: action.input.isRequired,
        isValid: action.input.isValid,
        identifier: action.input.identifier,
        errorMessage: action.input.errorMessage,
      };

      return {
        ...state,
        formFields: state.formFields.find(
          (field) => field.inputName === action.input.name
        )
          ? state.formFields.map((field) => {
              if (field.inputName === action.input.name) {
                return updatedInput;
              }

              return field;
            })
          : [...state.formFields, updatedInput],
      };

    default:
      return state;
  }
};
