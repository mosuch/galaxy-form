import React from "react";
import styled, { ThemeProvider } from "styled-components";
import { SpaceTravelForm } from "./routes/SpaceTravelForm";

const theme = {
  primaryColor: "#35588e",
  secondaryColor: "#1b3c67",
  tertiaryColor: "#f98f1e",
  quaternaryColor: "#A8AFB3",
  white: "#fff",
  grey: "#f7f7f7",
  greyAlternative: "#25395b",
  successPrimaryColor: "#effcf2",
  errorPrimaryColor: "#fab4b6",
  fontSmall: "12px",
  fontMedium: "14px",
  fontLarge: "22px",
};

export const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <StyledWrapper>
        <SpaceTravelForm />
      </StyledWrapper>
    </ThemeProvider>
  );
};

const StyledWrapper = styled.div`
  margin: 0 auto;
  width: 1200px;

  @media only screen and (max-width: 1240px) {
    width: 100%;
    box-sizing: border-box;
    padding: 0 15px;
  }
`;
