import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateForm } from "../../store/actions/form";
import { formFieldsSelector } from "../../store/selectors/form";

// It's not a hook, I know, but didn't have time to refactor it they way I planned initially
export const withValidation = (FieldComponent) => {
  return ({ name, identifier, validators, required, ...props }) => {
    const dispatch = useDispatch();
    const fields = useSelector(formFieldsSelector);
    const currentField = fields.find((f) => f.inputName === name) || {};

    const getFieldError = useCallback(
      (value) => {
        let errorMessage;

        for (let i = 0; i < validators.length; i++) {
          if (!validators[i].isSatisfied(value)) {
            errorMessage = validators[i].errorMessage;
            break;
          }
        }

        return errorMessage;
      },
      [validators]
    );

    const changeFieldHandler = ({ target }) => {
      dispatch(
        updateForm({
          name: target.name,
          value: target.value,
          isRequired: !!required,
          isValid: validators.every((v) => v.isSatisfied(target.value)),
          identifier,
          errorMessage: getFieldError(target.value),
        })
      );
    };

    return (
      <>
        <FieldComponent
          changeHandler={(e) => changeFieldHandler(e)}
          errorMessage={currentField?.errorMessage || ""}
          numberOfCharacters={currentField?.inputValue?.length}
          required={required}
          name={name}
          value={currentField?.inputValue}
          {...props}
        />
      </>
    );
  };
};
