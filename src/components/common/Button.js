import React from "react";
import styled from "styled-components";

export const Button = ({ text, clickHandler, disabled }) => {
  return (
    <StyledButton onClick={clickHandler} disabled={disabled}>
      {text}
    </StyledButton>
  );
};

const StyledButton = styled.button`
  line-height: 50px;
  width: 170px;
  color: #fff;
  border: 0;
  border-radius: 3px;
  background: linear-gradient(
    90deg,
    rgba(2, 0, 36, 1) 0%,
    rgba(237, 107, 36, 1) 0%,
    rgba(202, 30, 210, 1) 100%,
    rgba(0, 212, 255, 1) 100%
  );
  cursor: pointer;

  &:hover {
    filter: opacity(0.9);
    cursor: pointer;
  }

  &:disabled,
  &[disabled] {
    background: ${({ theme: { greyAlternative } }) => greyAlternative};
    cursor: not-allowed;
  }
`;
