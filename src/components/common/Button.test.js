import { Button } from "./Button";
import { render, screen, fireEvent } from "@testing-library/react";

describe("Button", () => {
  it("should render button", () => {
    render(<Button text="Click me" />);

    expect(
      screen.getByRole("button", {
        name: "Click me",
      })
    ).toBeInTheDocument();
  });

  it("should register click handler upon a click", () => {
    const clickfn = jest.fn();

    render(<Button text="Click me" clickHandler={clickfn} />);

    fireEvent.click(
      screen.getByRole("button", {
        name: "Click me",
      })
    );

    expect(clickfn).toHaveBeenCalledTimes(1);
  });
});
