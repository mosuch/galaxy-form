import React from "react";
import styled from "styled-components";

export const Input = ({
  minimumSize,
  maximumSize,
  label,
  placeholder,
  type,
  name,
  changeHandler,
  required,
  children,
  size,
  errorMessage,
  value,
  ...props
}) => {
  return (
    <StyledInput>
      <div className="input-container">
        <label className="input-label" htmlFor={name}>
          {label}
          {required ? "*" : null}
        </label>
        <input
          maxLength={maximumSize}
          minLength={minimumSize}
          placeholder={placeholder}
          type={type}
          id={name}
          name={name}
          onChange={changeHandler}
          size={size}
          required={required}
          className="text-input"
          value={value}
          {...props}
        />
        {children}
      </div>
      {errorMessage && (
        <div className="error">
          <span className="error__message">{errorMessage}</span>
        </div>
      )}
    </StyledInput>
  );
};

const StyledInput = styled.div`
  .input-container {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-top: 15px;
    flex-wrap: wrap;  
    
    & .text-input {
      flex-grow: 1;
      height: 28px;
      padding: 0 5px;
      border: 1px solid ${({ theme: greyAlternative }) => greyAlternative}
      color: ${({ theme: { primaryColor } }) => primaryColor};
    }
  
    & .input-label {
      color: ${({ theme: { quaternaryColor } }) => quaternaryColor};
      margin-right: 10px;
      min-width: 150px;
    }
  
    span {
      min-width: 150px;
      color: ${({ theme: { quaternaryColor } }) => quaternaryColor};
      font-size: ${({ theme: { fontSmall } }) => fontSmall}; 
      margin-left: 15px;
    }
  }

  .error {
    display: block;
    color: ${({ theme: { white } }) => white};
    text-align: right;
    font-size: ${({ theme: { fontSmall } }) => fontSmall};
    margin-top: 0;
    margin-top: 5px;
    
    &__message {
      border-radius: 2px;
      background: ${({ theme: { errorPrimaryColor } }) => errorPrimaryColor};
      padding: 2px 5px;
    }
  }
`;
