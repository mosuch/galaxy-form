import React, { useMemo } from "react";
import styled from "styled-components";

export const Dropdown = ({
  label,
  name,
  data,
  required,
  defaultOption = "Select from the list",
  changeHandler,
  errorMessage,
  value,
  ...props
}) => {
  const generateOptions = useMemo(() => {
    return data.map((val) => (
      <option value={val.id} key={val.id}>
        {val.name}
      </option>
    ));
  }, [data]);

  return (
    <StyledDropdown {...props}>
      <label className="label" htmlFor={label}>
        {label}
        {required ? "*" : null}
      </label>
      <select
        className="dropdown"
        name={name}
        id={name}
        required={required}
        onChange={changeHandler}
        value={value}
      >
        <option value="">{defaultOption}</option>
        {generateOptions}
      </select>
      {errorMessage && (
        <div className="error">
          <span className="error__message">{errorMessage}</span>
        </div>
      )}
    </StyledDropdown>
  );
};

const StyledDropdown = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-top: 15px;

  & .label {
    min-width: 150px;
    color: ${({ theme: { quaternaryColor } }) => quaternaryColor};
    margin-right: 10px;
  }

  & .dropdown {
    height: 28px;
    padding: 0 5px;
    border: 1px solid ${({ theme: { greyAlternative } }) => greyAlternative}
    color: ${(props) => props.theme.color};
  }

  .error {
    display: block;
    color: ${({ theme: { white } }) => white};
    text-align: right;
    font-size: ${({ theme: { fontSmall } }) => fontSmall};
    margin-top: 0;
    margin-left: 5px;
    
    &__message {
      border-radius: 2px;
      background: ${({ theme: { errorPrimaryColor } }) => errorPrimaryColor};
      padding: 2px 5px;
    }
  }
`;
