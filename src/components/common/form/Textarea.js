import React from "react";
import styled from "styled-components";

export const Textarea = ({
  placeholder,
  name,
  label,
  required,
  charsLimit,
  enableCounter = false,
  errorMessage,
  numberOfCharacters,
  changeHandler,
  value,
}) => {
  return (
    <StyledTextarea>
      <div className="textarea">
        <label htmlFor={name}>
          {label}
          {required ? "*" : null}
        </label>
        <textarea
          name={name}
          id={name}
          placeholder={placeholder}
          maxLength={charsLimit}
          onChange={changeHandler}
          required
          value={value}
        ></textarea>
      </div>
      {errorMessage && (
        <div className="error">
          <span className="error-message">{errorMessage}</span>
        </div>
      )}
      <div className="counter">
        {enableCounter ? (
          <span
            className={`characters-limit ${
              charsLimit === numberOfCharacters && "limit-exceeded"
            }`}
          >
            Maximum length of description: {numberOfCharacters} / {charsLimit}
          </span>
        ) : null}
      </div>
    </StyledTextarea>
  );
};

const StyledTextarea = styled.div`
  & .textarea {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    margin-top: 15px;

    & textarea {
      height: 200px;
      flex-grow: 1;
      font-family: inherit;
      font-size: ${({ theme: { fontMedium } }) => fontMedium};
      padding: 5px;
    }

    & label {
      min-width: 150px;
      color: ${({ theme: { quaternaryColor } }) => quaternaryColor};
      margin-right: 10px;
    }
  }

  .error {
    display: block;
    color: ${({ theme: { white } }) => white};
    text-align: right;
    font-size: ${({ theme: { fontSmall } }) => fontSmall};
    margin-top: 5px;

    & .error-message {
      border-radius: 2px;
      background: ${({ theme: { errorPrimaryColor } }) => errorPrimaryColor};
      padding: 2px 5px;
    }
  }

  & .counter {
    display: block;
    text-align: right;
    color: ${({ theme: { quaternaryColor } }) => quaternaryColor};
    font-size: ${({ theme: { fontSmall } }) => fontSmall};
    margin-top: 5px;

    & .limit-exceeded {
      border-radius: 2px;
      background: ${({ theme: { errorPrimaryColor } }) => errorPrimaryColor};
      padding: 2px 5px;
      color: ${({ theme: { white } }) => white};
    }
  }
`;
