import React from "react";
import styled from "styled-components";

export const RadioButton = ({
  radioValues,
  changeHandler,
  groupLabel,
  required,
}) => {
  return (
    <StyledRadio>
      <span className="group-label">
        {groupLabel}
        {required ? "*" : null}
      </span>
      {radioValues.map((radio, index) => {
        return (
          <React.Fragment key={index}>
            <input
              type="radio"
              id={radio.id}
              value={radio.value}
              name={radio.name}
              className="radio-input"
              onChange={changeHandler}
            />
            <label className="radio-element--label" htmlFor={radio.id}>
              {radio.displayName}
            </label>
          </React.Fragment>
        );
      })}
    </StyledRadio>
  );
};

const StyledRadio = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-top: 15px;

  & .group-label {
    min-width: 150px;
    color: ${({ theme: { quaternaryColor } }) => quaternaryColor};
    margin-right: 10px;
  }

  .radio-element,
  .radio-element--label {
    margin-right: 5px;
    text-transform: none;
  }

  .radio-element--label {
    margin-left: 5px;
  }
`;
