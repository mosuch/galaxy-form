import { Dropdown } from "./Dropdown";
import { render, screen } from "@testing-library/react";

describe("Dropdown", () => {
  it("should render dropdown options", () => {
    render(
      <Dropdown
        data-testid="dropdown"
        data={[
          { id: 1, name: "Option 1" },
          { id: 2, name: "Option 2" },
        ]}
      />
    );

    expect(screen.getByTestId("dropdown")).toBeInTheDocument();
    expect(
      document.querySelectorAll('[data-testid="dropdown"] option')[0].innerHTML
    ).toBe("Select from the list");
    expect(
      document.querySelectorAll('[data-testid="dropdown"] option')[1].innerHTML
    ).toBe("Option 1");
    expect(
      document.querySelectorAll('[data-testid="dropdown"] option')[2].innerHTML
    ).toBe("Option 2");
  });
});
