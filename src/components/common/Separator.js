import React from 'react';
import styled from 'styled-components';

export const Separator = () => {
  return <StyledSeparator />;
};

const StyledSeparator = styled.hr`
  border: 1px solid ${({ theme: { grey } }) => grey};
  margin: 5px 0;
`;
