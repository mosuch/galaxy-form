import React from "react";
import styled from "styled-components";

export const Card = ({ children }) => {
  return <StyledCard>{children}</StyledCard>;
};

const StyledCard = styled.div`
  background: ${({ theme: { white } }) => white};
  box-shadow: 0px 1px 3px 1px rgba(0, 0, 0, 0.2);
  margin-top: 40px;
  padding: 20px;
  border-radius: 20px;
`;
