import React from "react";
import styled from "styled-components";
import { Card } from "../components/common/Card";
import { Textarea } from "../components/common/form/Textarea";
import { Input } from "../components/common/form/Input";
import { Separator } from "../components/common/Separator";
import { Dropdown } from "../components/common/form/Dropdown";
import { RadioButton } from "../components/common/form/Radio";
import { Button } from "../components/common/Button";
import {
  notEmptyValidator,
  lengthValidator,
  urlValidator,
  numberValidator,
  imageValidator,
} from "../helpers/validators";
import { sendForm } from "../store/actions/form";
import {
  isFormValidSelector,
  submissionsSelector,
} from "../store/selectors/form";
import { withValidation } from "../components/hooks/useValidation";
import { useSelector, useDispatch } from "react-redux";
import { SubmissionsList } from "./Form/Submissions/SubmissionsList";

const galaxiesList = [
  {
    key: "1",
    name: "Andromeda",
    value: "Andromeda",
  },
  {
    key: "2",
    name: "Cigar",
    value: "Cigar",
  },
  {
    key: "3",
    name: "Magellanic Cloud",
    value: "Magellanic Cloud",
  },
  {
    key: "4",
    name: "Pinwheel",
    value: "Pinwheel",
  },
  {
    key: "5",
    name: "NGC 1300",
    value: "NGC 1300",
  },
  {
    key: "6",
    name: "Tadpole",
    value: "Tadpole",
  },
];

const firstNameValidator = [notEmptyValidator, lengthValidator(2, 30)];
const travelReasonValidator = [notEmptyValidator, lengthValidator(10, 140)];
const travelDurationValidator = [notEmptyValidator, numberValidator];
const destinationGalaxyValidator = [notEmptyValidator];
const spaceDrivingLicenseValidator = [notEmptyValidator];
const travelerIDValidator = [notEmptyValidator, urlValidator, imageValidator];

const FirstName = withValidation(Input);
const TravelReason = withValidation(Textarea);
const DestinationGalaxy = withValidation(Dropdown);
const TravelDuration = withValidation(Input);
const SpaceDrivingLicense = withValidation(RadioButton);
const TravelerID = withValidation(Input);

export const SpaceTravelForm = () => {
  const dispatch = useDispatch();
  const isFormValid = useSelector(isFormValidSelector);
  const submissionsList = useSelector(submissionsSelector);

  const submitForm = (e) => {
    e.preventDefault();
    dispatch(sendForm());
  };

  return (
    <>
      <Card data-testid="space-travel-form">
        <StyledForm>
          <>
            <section>
              <header>
                <h2>Space travel form</h2>
                <p>Use this form to apply for space travel permission</p>
              </header>
              <Separator />
              <FirstName
                type="text"
                maximumSize={80}
                minimumSize={10}
                label="Name"
                placeholder="Your name written in common language"
                name="name"
                identifier="name"
                required
                validators={firstNameValidator}
              />
              <TravelReason
                placeholder="Type down your motivations; why you want to leave the Earth?"
                name="travelReason"
                label="Why?"
                enableCounter
                charsLimit={140}
                identifier="travelReason"
                validators={travelReasonValidator}
                required
              />
              <DestinationGalaxy
                label="Destination Galaxy"
                name="destinationGalaxy"
                data={galaxiesList}
                defaultOption="Select galaxy"
                identifier="galaxy_id"
                validators={destinationGalaxyValidator}
                data-testid="destination-galaxy"
                required
              />
              <TravelerID
                type="text"
                label="Traveler ID scan"
                name="travelerId"
                placeholder="URL to your ID scan"
                identifier="travelerId"
                validators={travelerIDValidator}
                required
              />
              <SpaceDrivingLicense
                groupLabel="SDL?"
                radioValues={[
                  {
                    id: "yes",
                    name: "spaceDrivingLicense",
                    value: "yes",
                    displayName: "Yes",
                  },
                  {
                    id: "no",
                    name: "spaceDrivingLicense",
                    value: "no",
                    displayName: "No",
                  },
                ]}
                identifier="spaceDrivingLicense"
                validators={spaceDrivingLicenseValidator}
                required
              />
              <TravelDuration
                label="Travel duration"
                name="travelDuration"
                placeholder="Numbers only (years)"
                type="text"
                identifier="travelDuration"
                validators={travelDurationValidator}
                required
              />
            </section>
            <div className="button-holder">
              <Button
                text="Submit"
                type="submit"
                clickHandler={(e) => submitForm(e)}
                disabled={!isFormValid}
              />
            </div>
          </>
        </StyledForm>
      </Card>
      {!!submissionsList.length && (
        <SubmissionsList submissions={submissionsList} />
      )}
    </>
  );
};

const StyledForm = styled.form`
  margin-bottom: 20px;

  & h2 {
    color: ${({ theme: { primaryColor } }) => primaryColor};
  }

  & .button-holder {
    margin-top: 20px;
    display: flex;
    justify-content: center;
  }
`;
