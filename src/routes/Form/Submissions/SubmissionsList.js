import React from "react";
import { Card } from "../../../components/common/Card";
import { SubmissionItem } from "../Submissions/SubmissionItem";

export const SubmissionsList = ({ submissions }) => (
  <Card>
    <h2>Previous submissions</h2>
    {submissions.map((submission, index) => {
      return <SubmissionItem key={index} submission={submission} />;
    })}
  </Card>
);
