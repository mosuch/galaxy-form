import React from "react";
import styled from "styled-components";

export const SubmissionItem = ({ submission }) => (
  <Tile>
    <p>Name: {submission.name}</p>
    <p>Travel reason: {submission.travelReason}</p>
    <p>Destination galaxy: {submission.destinationGalaxy}</p>
    <p>Traveler ID scan: {submission.travelerId}</p>
    <p>Has space driving license?: {submission.spaceDrivingLicense}</p>
    <p>Travel duration: {submission.travelDuration}</p>
  </Tile>
);

const Tile = styled.div`
  padding: 10px;
  background: #87ceeb;
  border-radius: 5px;
  margin-bottom: 15px;
`;
